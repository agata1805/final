const searchInput = document.getElementById('inputSearch');
const suggestions = document.getElementById('suggestions');

searchInput.addEventListener('change', displayMatches);
searchInput.addEventListener('keyup', displayMatches);
searchInput.addEventListener('keypress', displayMatches);
searchInput.addEventListener('click', removeMatches);

const breeds = [];

fetch(`https://api.thedogapi.com/v1/breeds`)
    .then(response => response.json())
    .then(data => {breeds.push(...data)});

function displayMatches() {
    const matchArray = findMatches(this.value, breeds);
    const suggestBreed = matchArray.map(dog => {
    const regex = new RegExp(this.value, 'gi');
    const dogName = dog.name.replace(regex, `<span class="text--highlighted">${this.value}</span>`);
    return `
    <li class="list__item">
      <span id="dogSearch">${dogName}</span>
    </li>
    `;
    }).join('');
    suggestions.innerHTML = suggestBreed;
  
    const matchBreed = document.querySelectorAll('.list__item');
    
    matchBreed.forEach(function(item, index){
        item.addEventListener('click', function(){
            let resultId = matchArray[index].id;
            fetchDogApi(dogKey, resultId);
                suggestions.innerHTML = '';
        });
      });
}

function findMatches(wordToMatch, breeds) {
 return breeds.filter(dog => {
   const regex = new RegExp(wordToMatch, 'gi');
   return dog.name.match(regex)
 })
}

function removeMatches() {
    let ignoreClickOnMeElement = suggestions;

    document.addEventListener('click', function(event) {
        let isClickInsideElement = ignoreClickOnMeElement.contains(event.target);
        if (!isClickInsideElement) {
            suggestions.innerHTML = '';
        }
    });
}

// -----------------------------------------
function fetchDogApi(dogKey, dogId) {

    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("x-api-key", dogKey);

    let requestOptions = {
        method: 'GET',
    };

 fetch(`https://api.thedogapi.com/v1/breeds/${dogId}`, requestOptions)
   .then(response => response.json())
   .then(data => {
   let breedName =  data.name;
   let temperament = data.temperament;
   let dogWeight =  data.weight.metric;
   let dogSize = data.height.metric;
   let dogLifespan = data.life_span;
   let dogOrigin =  data.origin;
   let bredFor = data.bred_for;
   let icon = data.id;

   document.getElementById('dogBreed').innerHTML = breedName;
   document.getElementById('dogTemperament').innerHTML = temperament;
   document.getElementById('dogWeight').innerHTML = `${dogWeight} kg`;
   document.getElementById('dogSize').innerHTML = `${dogSize} cm`;
   document.getElementById('dogLifespan').innerHTML = dogLifespan;
   document.getElementById('dogIcon').src = getDogBreedIcon(icon);
    if(dogOrigin === undefined) {
        document.getElementById('dogOrigin').innerHTML = '-';
    }
    if(typeof bredFor !== 'undefined') {
        document.getElementById('dogFacts').innerHTML = bredFor;
    } else {
        document.getElementById('dogOrigin').innerHTML = dogOrigin;
    }
    if(bredFor === undefined) {
        document.getElementById('dogFacts').innerHTML = '-';
    } else {
        document.getElementById('dogFacts').innerHTML = bredFor;
    }
   })
   .catch(error => showError('Choose a dog'));
}
function showError () {
    const div = document.createElement('div');
    div.className = 'error-message';
    div.appendChild(document.createTextNode('Sorry, something went wrong'));
    const container = document.querySelector('.app__container');
    const header = document.querySelector('.app__header');
    container.insertBefore(div, header);

    setTimeout(function () {
        document.querySelector('.error-message').remove();
    }, 3200);
}

function getRandomDog() {
    const keys = breeds.length;
    let randomIndex = Math.floor(Math.random() * keys);
    fetchDogApi(dogKey, randomIndex);
}

document.getElementById('searchRandom').addEventListener('click', getRandomDog);

function getDogBreedIcon(icon) {
 switch (icon) {
     case iconValue.Afghan_hound:
         return "img/dogBreeds/afghan-hound.png";
     case iconValue.Airedale:
         return "img/dogBreeds/airedale.png";
     case iconValue.Akita:
         return "img/dogBreeds/akitas.png";
     case iconValue.Alaskan_husky:
         return "img/dogBreeds/husky.png";
     case iconValue.American_staffordshire:
         return "img/dogBreeds/american-staffordshire-terrier.png";
     case iconValue.Australian_shepherd:
         return "img/dogBreeds/australian-shepherd.png";
     case iconValue.Basset_hound:
         return "img/dogBreeds/basset-hound.png";
     case iconValue.Basenji:
         return "img/dogBreeds/Basenji.png";
     case iconValue.Beagle:
         return "img/dogBreeds/beagle.png";
     case iconValue.Bedlington_terrier:
         return "img/dogBreeds/bedlington-terrier.png";
     case iconValue.Bernese_mountain:
         return "img/dogBreeds/bernese-mountain.png";
     case iconValue.Belgian_malinois:
         return "img/dogBreeds/Belgian-Malinois.png";
     case iconValue.Bichon_frise:
         return "img/dogBreeds/bichon-frise.png";
     case iconValue.Border_collie:
         return "img/dogBreeds/border-collie.png";
     case iconValue.Boston_terrier:
         return "img/dogBreeds/boston-terrier.png";
     case iconValue.Boxer:
         return "img/dogBreeds/boxer.png";
     case iconValue.Bullmastiff:
         return "img/dogBreeds/bullmastiff.png";
     case iconValue.Bullterrier:
         return "img/dogBreeds/bullterrier.png";
     case iconValue.Chihuahua:
         return "img/dogBreeds/chihuahua.png";
     case iconValue.Chinese_crested:
         return "img/dogBreeds/chinese-crested.png";
     case iconValue.Chow_chow:
         return "img/dogBreeds/chow-chow.png";
     case iconValue.Collie:
         return "img/dogBreeds/collie.png";
     case iconValue.Corgi:
         return "img/dogBreeds/corgi.png";
     case iconValue.Dachshund:
         return "img/dogBreeds/dachshund.png";
     case iconValue.Dalmatian:
         return "img/dogBreeds/dalmatian.png";
     case iconValue.Doberman:
         return "img/dogBreeds/doberman.png";
     case iconValue.English_cocker_spaniel:
         return "img/dogBreeds/english-cocker-spaniel.png";
     case iconValue.French_bulldog:
         return "img/dogBreeds/french-bulldog.png";
     case iconValue.German_shepherd:
         return "img/dogBreeds/german-shepherd.png";
     case iconValue.Golden_retriever:
         return "img/dogBreeds/golden-retriever.png";
     case iconValue.Great_dane:
         return "img/dogBreeds/great-dane.png";
     case iconValue.Great_pyrenees:
         return "img/dogBreeds/Great-Pyrenees.png";
     case iconValue.Greyhound:
         return "img/dogBreeds/greyhound.png";
     case iconValue.Havanese:
         return "img/dogBreeds/Havanese.png";
     case iconValue.Siberian_husky:
         return "img/dogBreeds/husky.png";
     case iconValue.Jack_russel_terrier:
         return "img/dogBreeds/jack-russell-terrier.png";
     case iconValue.Japanese_chin:
         return "img/dogBreeds/japanese-chin.png";
     case iconValue.Kurzhaar:
         return "img/dogBreeds/kurzhaar.png";
     case iconValue.Labrador_retriever:
         return "img/dogBreeds/labrador.png";
     case iconValue.Malamute:
         return "img/dogBreeds/malamute.png";
     case iconValue.Maltese:
         return "img/dogBreeds/maltese.png";
     case iconValue.Miniature_Schnauzer:
         return "img/dogBreeds/miniature-schnauzer.png";
     case iconValue.Newfoundland:
         return "img/dogBreeds/newfoundland.png";
     case iconValue.Pharaoh_hound:
         return "img/dogBreeds/pharaoh-hound.png";
     case iconValue.Pit_bull:
         return "img/dogBreeds/pit-bull.png";
     case iconValue.Pomeranian:
         return "img/dogBreeds/pomeranian.png";
     case iconValue.Poodle:
         return "img/dogBreeds/poodle.png";
     case iconValue.Poodle_mini:
         return "img/dogBreeds/poodle.png";
     case iconValue.Pug:
         return "img/dogBreeds/pug.png";
     case iconValue.Rhodesian_ridgeback:
         return "img/dogBreeds/ridgeback.png";
     case iconValue.Rottweiler:
         return "img/dogBreeds/rottweiler.png";
     case iconValue.Saluki:
         return "img/dogBreeds/saluki.png";
     case iconValue.Scottish_terrier:
         return "img/dogBreeds/yorkshire-terrier.png";
     case iconValue.Sealyham_terrier:
         return "img/dogBreeds/yorkshire-terrier.png";
     case iconValue.Shar_pei:
         return "img/dogBreeds/shar-pei.png";
     case iconValue.Sheltie:
         return "img/dogBreeds/sheltie.png";
     case iconValue.Shiba_inu:
         return "img/dogBreeds/shiba-inu.png";
     case iconValue.Shihpoo:
         return "img/dogBreeds/shih-tzu.png";
     case iconValue.Shih_tzu:
         return "img/dogBreeds/shih-tzu.png";
     case iconValue.Springer_spaniel:
         return "img/dogBreeds/springer-spaniel.png";
     case iconValue.St_bernard:
         return "img/dogBreeds/st-bernard.png";
     case iconValue.Thai_ridgeback:
         return "img/dogBreeds/ridgeback.png";
     case iconValue.Tibetan_mastiff:
         return "img/dogBreeds/tibetan-mastiff.png";
     case iconValue.Xoloitzcuintli:
         return "img/dogBreeds/xoloitzcuintli.png";
     case iconValue.Yorkshire_terrier:
         return "img/dogBreeds/yorkshire-terrier.png";
     case iconValue.Aw_spaniel:
         return "img/dogBreeds/english-cocker-spaniel.png";
     case iconValue.Boykin_spaniel:
         return "img/dogBreeds/english-cocker-spaniel.png";
     case iconValue.Cavalier_spaniel:
         return "img/dogBreeds/english-cocker-spaniel.png";
     case iconValue.Clumber_spaniel:
         return "img/dogBreeds/english-cocker-spaniel.png";
     case iconValue.A_spaniel:
         return "img/dogBreeds/english-cocker-spaniel.png";
     case iconValue.English_spaniel:
         return "img/dogBreeds/english-cocker-spaniel.png";
     case iconValue.English_toy_spaniel:
         return "img/dogBreeds/english-cocker-spaniel.png";
     case iconValue.Field_spaniel:
         return "img/dogBreeds/english-cocker-spaniel.png";
     case iconValue.Tibetan_spaniel:
         return "img/dogBreeds/english-cocker-spaniel.png";
     case iconValue.Welsh_spaniel:
         return "img/dogBreeds/english-cocker-spaniel.png";
     default:
         return "img/pawprint%20(2).png";
 }
}

